# Cookie-cutter template for a Python package

## Installation

```bash
pip install cookiecutter
```

## Usage

```bash
cookiecutter https://gitlab.math.unistra.fr/boileau/cookiecutter.git --directory python
```

You will be prompted for some information about this new project.

What it does:

- Creates a python project skeleton containing the basic files for:
    - python module source code
    - `pyproject.toml` file for packaging
    - [sphinx](https://www.sphinx-doc.org/) documentation
    - unitary tests with [pytest](https://docs.pytest.org/)
- Setup a `.gitlab-ci.yml` file for continuous integration
- Initializes a git repository
- Creates a new remote repository on Gitlab
- Pushes the local repository to the remote repository, which triggers the CI pipeline (tests are performed and the documentation is published on [Gitlab pages](https://docs.gitlab.com/ee/user/project/pages/))

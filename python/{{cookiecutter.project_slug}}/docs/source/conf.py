# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information
from {{ cookiecutter.project_slug }} import __version__

project = "{{ cookiecutter.project_slug }}"
copyright = "{% now 'local', '%Y' %} {{ cookiecutter.full_name }}"
author = "{{ cookiecutter.full_name }}"
release = __version__

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "myst_parser",
    "sphinx.ext.autodoc",
    "sphinx.ext.napoleon",
    "sphinx.ext.viewcode",
    "sphinx_copybutton",
    "sphinx.ext.autosummary",
]

templates_path = ["_templates"]
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "furo"

autodoc_default_options = {
    "members": True,
    "show-inheritance": True,
    "member-order": "groupwise",
    "private-members": False,
    "special-members": "__init__",
}

autosummary_generate = True  # Turn on sphinx.ext.autosummary
add_module_names = False  # Remove module names from class/method signatures

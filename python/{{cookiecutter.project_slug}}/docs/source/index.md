# {{ cookiecutter.project_name }}

[![pipeline status]({{ cookiecutter.gitlab_project_url }}/badges/main/pipeline.svg)]({{ cookiecutter.gitlab_project_url }}/-/commits/main)
[![coverage report]({{ cookiecutter.gitlab_project_url }}/badges/main/coverage.svg)]({{ cookiecutter.gitlab_pages_url }}/coverage)
[![Latest Release]({{ cookiecutter.gitlab_project_url }}/-/badges/release.svg)]({{ cookiecutter.gitlab_project_url }}/-/releases)

## Description

{{ cookiecutter.project_short_description }}

## Features

- everything has
- to be done

## Installation instructions

```{toctree}
:maxdepth: 2
installation.md
```

## Modules

```{eval-rst}
.. autosummary::
   :toctree: _autosummary
   :caption: Modules
   :recursive:

   {{ cookiecutter.project_slug }}.{{ cookiecutter.project_slug }}
```

## Indices and tables

* {ref}`genindex`
* {ref}`modindex`

# {{ cookiecutter.project_name }}

[![pipeline status]({{ cookiecutter.gitlab_project_url }}/badges/main/pipeline.svg)]({{ cookiecutter.gitlab_project_url }}/-/commits/main)
[![coverage report]({{ cookiecutter.gitlab_project_url }}/badges/main/coverage.svg)]({{ cookiecutter.gitlab_pages_url }}/coverage)
[![Latest Release]({{ cookiecutter.gitlab_project_url }}/-/badges/release.svg)]({{ cookiecutter.gitlab_project_url }}/-/releases)
[![Doc](https://img.shields.io/badge/doc-sphinx-blue)]({{ cookiecutter.gitlab_pages_url }})


{{ cookiecutter.project_short_description }}

## Features

* TODO
* More TODO

See [Documentation]({{ cookiecutter.gitlab_pages_url }}) for details.

import pytest
from {{cookiecutter.project_slug}} import {{cookiecutter.project_slug}}


@pytest.fixture
def response():
    """Sample pytest fixture.

    See more at: http://doc.pytest.org/en/latest/fixture.html
    """
    pass


def test_content(response):
    """Sample pytest test function with the pytest fixture as an argument."""
    pass

#!/bin/bash

git init
git add .
git commit -m "First commit from cookiecutter" -m "Generated from https://gitlab.math.unistra.fr/mboileau/cookiecutter.git"
git remote add origin {{ cookiecutter.gitlab_remote }}
git push --set-upstream origin main
